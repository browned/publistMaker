from __future__ import division, print_function
from time import localtime
from datetime import datetime, timedelta
import numpy as np
import ads
from .formatting import CV_format_entry, FormD_format_entry

class publistMaker:
    """
    wrapper class to assemble tex-formatted publication and citation records
    """
    
    def __init__(self,**kwargs):
        """
        constructor
        
        Parameters
        ----------
        **kwargs : optional
            flags to pass to ads.SearchQuery.  At present these are just basic
            parameters like name, orcid, date...
        """

        if 'sort' not in kwargs:
            kwargs['sort'] = 'date'
        print('retrieving records from ads with search: '+', '.join('{0} = {1}'.format(k,v) for k, v in kwargs.items()))
        filters = {
            'allarticles':'property:article',
            'refereed':'property:refereed AND property:article',
            'unrefereed':'property:notrefereed AND property:article',
            'contributed':'property:notrefereed AND property:nonarticle'
        }
        req_fields = [ 'id','author','bibcode','title','page','pub','year',
        'volume','citation_count','property','first_author','aff','pubdate' ]
        self.refereed = list(ads.SearchQuery(fq=filters['refereed'],fl=req_fields,**kwargs))
        print('got {} refereed'.format(len(self.refereed)))
        self.unrefereed = list(ads.SearchQuery(fq=filters['unrefereed'],fl=req_fields,**kwargs))
        print('got {} unrefereed'.format(len(self.unrefereed)))
        self.contributed = list(ads.SearchQuery(fq=filters['contributed'],fl=req_fields,**kwargs))
        print('got {} contributed'.format(len(self.contributed)))
        self.allarticles = list(ads.SearchQuery(fq=filters['allarticles'],fl=req_fields,**kwargs))
        print('got {} allarticles'.format(len(self.allarticles)))
        self.have_metrics=False

    def FormD(self,pubtype='allarticles',supervisors='',my_name='nobody',date_last_action='0000-00-00',underline_first=True):
        """
        Returns generator for iterating over FormD publication list of refereed
        entries.
        
        Parameters
        ----------
        pubtype : str, optional
            type of list.  Options are 'refereed', 'unrefereed', and 
            'contributed'
        supervisors : set(str), optional
            set of strings holding last names of supervisors, e.g., 
            {'Bildsten','Truran','Rosner','Lamb'}
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        date_last_action : str, optional
            Date of last action (hiring/reappointment/promotion) in form
            YYYY-MM-DD.
        underline_first : boolean, optional
            If true, presume the first author is the lead, as conventional in
            astronomy. If false, there is no underlining, as one cannot
            determine the lead author from the information in the bibliographic
            information.
        """
        
        if pubtype == 'refereed':
            l = self.refereed
        elif pubtype == 'unrefereed':
            l = self.unrefereed
        elif pubtype == 'contributed':
            l = self.contributed
        elif pubtype == 'allarticles':
            l = self.allarticles
        else:
            print('unknown pubtype {}'.format(pubtype))
            return None
        
        return ( FormD_format_entry(entry,supervisors=supervisors,my_name=my_name, date_last_action=date_last_action,underline_first=underline_first) for entry in l )
    
    def CV(self,pubtype='refereed',max_authors=8,etal=3,list_cites=True,my_name='nobody'):
        """
        Returns generator for iterating over tex-format listing of publications.
        
        Parameters
        ----------
        pubtype : str, optional
            type of list.  Options are 'refereed', 'unrefereed', and 
            'contributed'
        max_authors : int, optional
            Maximum number of authors in list [default 8]
        etal : int, optional
            If author list truncated, number of authors listed before "et al."
            [default 3]
        list_cites : boolean, optional
            if True [default] list the number of citations following the page.
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        """
        
        if pubtype == 'refereed':
            l = self.refereed
        elif pubtype == 'unrefereed':
            l = self.unrefereed
        elif pubtype == 'contributed':
            l = self.contributed
        elif pubtype == 'allarticles':
            l = self.allarticles
        else:
            print('unknown pubtype {}'.format(pubtype))
            return None
        
        return ( CV_format_entry(entry,max_authors=max_authors,etal=etal,list_cites=list_cites,my_name=my_name) for entry in l)

    def get_metrics(self,number_years=12,end_year='now',my_name='nobody'):
        """
        Compiles publication and citation metrics for refereed publications
        
        Parameters
        ----------
        number_years : int, optional
            Span of years for which citations by year and publications per year
            are reported
        end_year : str, optional
            if 'now' the current year is used.  Otherwise, one may supply a 
            value, e.g. '2004'.
        my_name : str, optional
            String used for tagging your papers, e.g. 'Brown, E'. Note that
            this string must be a substring of the listing in surname, initial
            format.
        """

        # set the range of years over which to generate the histogram.
        end_year = localtime().tm_year if end_year == 'now' else int(end_year)
        first_year = end_year-number_years+1

        # count refereed publications per year
        self.first_author_pubs = np.zeros(number_years,dtype=int)
        self.coauthor_pubs = np.zeros(number_years,dtype=int)
        for paper_id,paper in enumerate(self.refereed):
            this_paper_year_id = int(paper.year)-first_year
            first_author = my_name in paper.first_author
            if this_paper_year_id >= 0 and this_paper_year_id < number_years:
                if first_author:
                    self.first_author_pubs[this_paper_year_id] += 1
                else:
                    self.coauthor_pubs[this_paper_year_id] += 1

        # count citations to refereed articles per year
        # get bibcodes for refereed articles
        bibcodes = [ a.bibcode for a in self.refereed ]
        mq = ads.MetricsQuery(bibcodes=bibcodes)
        metrics = mq.execute()
        self.citations = np.zeros(number_years,dtype=int)
        self.years = [ str(y) for y in range(first_year,first_year+number_years) ]

        for y in range(number_years):
            year = str(y+first_year)
            self.citations[y] += metrics['histograms']['citations']['refereed to refereed'][year]
            self.citations[y] += metrics['histograms']['citations']['nonrefereed to refereed'][year]

        # citations per article is not currently implemented
        self.citations_per_article = np.zeros(shape=(len(self.refereed),number_years),dtype=int)    
        self.have_metrics=True

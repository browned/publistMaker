# publistMaker

A python tool for pulling publication records from ADS and compiling them into
a variety of formats, including that of MSU's FormD (for promotion & tenure).

## Dependencies

0.	Python 2.11 or later.  You will need the `pip` installer.
1.	ads python module
2.	numpy
3.	developer API key from ads
4.	[optional] An Orc ID, which allows you to tag your articles.

## Installation

1.	From within the top-level directory (containing `setup.py`), execute

        pip install .

    This will install the publistMaker class files into your distribution and install the `ads` module if necessary.

2.	Obtain and install an ADS API key.
	1.	Go to https://ui.adsabs.harvard.edu
	2.	Select 'account settings'. (You will need to sign in or make a new account.  Note that this account is different than any you might have with ADS classic.)  This may be under the menu item 'customize settings'
	3.	Select 'API Token' and follow the instructions to generate a new key.
	4.	Save this key to a file `~/.ads/dev_key`.

3.	The most convenient way to tag your articles is to obtain an [ORCID](https://orcid.org/).  You can then register this key with ADS (may not be usable until the next day).  You then search for your papers and tag them with your ORCID. Note that at present, the class will only do simple searches, e.g., name or ORCID.  This may be improved in the near future.

4.	Look at the `samples/example_publist.py` script for an example of how to generate a latex file containing your publications in reverse chronological order.  The files `samples/publication-list.tex` and `samples/publication-report.tex` show how to incorporate the output into a tex report.

5.	The ADS entries are stored at UTF-8 characters.  In order to process non-ASCII characters in names or Greek symbols in the title, you should typeset the generated list with xelatex, or by putting `\use{textcomp}` in the preamble.  The script will replace html directives such as `<SUP>` in the title, but this is not guaranteed to be complete.

6.	Look at `samples/metric_plots.py` for an example of how to generate and plot citation statistics.

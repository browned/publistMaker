from __future__ import print_function, division
from publistmaker import publistMaker
import codecs
import sys

my_supervisors = ('Bildsten','Truran','Lamb','Rosner')
my_orcid = '0000-0003-3806-5339'
p = publistMaker(orcid=my_orcid,max_pages=10)

beg_list = r'\begin{etaremune}' + '\n\n'
end_list = r'\end{etaremune}' + '\n\n'

def progress(stem,fraction_done):
    print('{0}: {1:2.0%}'.format(stem,fraction_done),end='\r')
    sys.stdout.flush()

cnt=0
with codecs.open('formD.tex',encoding='utf-8',mode='w') as f:
    f.write(beg_list)
    for paper in p.FormD(supervisors=my_supervisors,my_name='Brown, E'):
        f.write(paper)
        cnt += 1
        progress('writing formD.tex',cnt/len(p.refereed))
    f.write(end_list)
print("\n")

cnt=0
with codecs.open('refereed-publication-list.tex',encoding='utf-8',mode='w') as f:
    f.write(beg_list)
    for paper in p.CV():
        f.write(paper)
        cnt += 1
        progress('writing refereed-publication-list.tex',cnt/len(p.refereed))
    f.write(end_list)
print("\n")
